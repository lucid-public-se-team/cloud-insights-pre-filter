# Cloud Insights Pre-Filter

A Python Script that will allow you to filter a JSON file before uploading it to Lucidchart Cloud Insights

To run the script you would simply call:
python filter_aws.py
from there it will walk you through pointing the script at a JSON file, it will then print out all of the tags that are being used and ask you which tag you would like to filter on, it will then ask you for an output file name and filter the JSON.

You can use OR logic to combine filters as part of the script's functionality. In order to use AND logic, simply run the script multiple times.
