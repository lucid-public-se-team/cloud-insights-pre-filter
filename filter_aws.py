import json
import pprint

pp = pprint.PrettyPrinter(indent=4)

def filterPrep():
    input_file_name = input("Enter the name of your JSON file: ")
    cloud_infra = {}
    with open(input_file_name) as input_file: #TODO handle a failure better
        cloud_infra = json.load(input_file)
        account_num = 1
        if len(cloud_infra.get('accounts')) > 1:
            for account in cloud_infra.get('accounts'): #Setup filtering at the account level
                if account.get('accountAliases'):
                    print("{0}: {1}".format(account_num, account.get('accountAliases')[0]))
                else:
                    print("{0}: {1}".format(account_num, account.get('accountId')))
                account_num+=1
            print("{0}: all accounts".format(account_num))
            print("Which account would you like to filter?")
            print("Choosing which tags to filter on will happen for each account choose to filter all accounts.")
            account_selection = input("Please select a number between 1 and {0}: ".format(account_num))
            account_selection = int(account_selection) #TODO handle failure nicely
            if account_selection == account_num: #Filter all accounts
                for account in cloud_infra.get('accounts'):
                    account = filterAccount(account)
            else: #Filter only a specific account
                account = cloud_infra.get('accounts')[account_selection-1]
                account = filterAccount(account)
        else:
            for account in cloud_infra.get('accounts'):
                account = filterAccount(account)
    output_file_name = input("Enter the desired output file name: ")
    with open(output_file_name, "w") as outfile:
        outfile.write(json.dumps(cloud_infra, indent = 4))

    print("Your file was writen to {0}".format(output_file_name))

def filterAccount(account):
    account_tags = gatherTags(account)
    print("Please see your tags below: ")
    pp.pprint(account_tags)
    keep_going = "yes"
    filters = []
    while ("yes" in keep_going) or ("y" in keep_going):
        filter_key = input("\nPlease choose the tag that you would like to filter on (tag value will be input at another time): ")
        filter_value = input("Please choose the tag value that you would like to filter on: ")
        filters.append({"key": filter_key, "value": filter_value})
        print("\nType 'yes' or 'y' to add another tag to the filter. Type anything else to move on.")
        keep_going = input("The filter will use 'OR' logic. To use 'AND' logic simply run the script again: ")
    account = filterByTag(account, filters)
    return account


def filterByTag(account, filters):
    for hostedZone in account.get("resources").get("route53").get("hostedZones"):
        found = False
        for tag in hostedZone.get("Tags"):
            tag_found = checkForTag(tag, filters)
            if tag_found:
                found = True
        if not found:
            account.get("resources").get("route53").get("hostedZones").remove(hostedZone)

    for cloudFront in account.get("resources").get("cloudFront").get("distributions"):
        found = False
        for tag in cloudFront.get("Tags").get("Items"):
            tag_found = checkForTag(tag, filters)
            if tag_found:
                found = True
        if not found:
            account.get("resources").get("cloudFront").get("distributions").remove(cloudFront)

    for customerGateway in account.get("resources").get("ec2").get("customerGateways"):
        found = False
        for tag in customerGateway.get("Tags"):
            tag_found = checkForTag(tag, filters)
            if tag_found:
                found = True
        if not found:
            account.get("resources").get("ec2").get("customerGateways").remove(customerGateway)

    for vpcPeeringCon in account.get("resources").get("ec2").get("vpcPeeringConnections"):
        found = False
        for tag in vpcPeeringCon.get("Tags"):
            tag_found = checkForTag(tag, filters)
            if tag_found:
                found = True
        if not found:
            account.get("resources").get("ec2").get("vpcPeeringConnections").remove(vpcPeeringCon)

    for vpnGateway in account.get("resources").get("ec2").get("vpnGateways"):
        found = False
        for tag in vpnGateway.get("Tags"):
            tag_found = checkForTag(tag, filters)
            if tag_found:
                found = True
        if not found:
            account.get("resources").get("ec2").get("vpnGateways").remove(vpnGateway)

    for vpnCon in account.get("resources").get("ec2").get("vpnConnections"):
        found = False
        for tag in vpnCon.get("Tags"):
            tag_found = checkForTag(tag, filters)
            if tag_found:
                found = True
        if not found:
            account.get("resources").get("ec2").get("vpnConnections").remove(vpnCon)

    for region in account.get("regions"):
        resources = region.get("resources")
        for elb in resources.get("elb").get("loadBalancers"):
            found = False
            for tag in elb.get("Tags"):
                tag_found = checkForTag(tag, filters)
                if tag_found:
                 found = True
            if not found:
                resources.get("elb").get("loadBalancers").remove(elb)

        for apigate in resources.get("apigatewayv2").get("apis"):
            found = False
            for Key, Value in apigate.get("Tags").items():
                tag = {"Key": Key, "Value": Value}
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("elb").get("apigatewayv2").get("apis").remove(apigate)

        for cluster in resources.get("rds").get("dbClusters"):
            found = False
            for tag in cluster.get("TagList"):
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("rds").get("dbClusters").remove(cluster)

        for instance in resources.get("rds").get("dbInstances"):
            found = False
            for tag in instance.get("TagList"):
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("rds").get("dbInstances").remove(instance)

        for queue in resources.get("sqs").get("queues"):
            found = False
            for Key, Value in queue.get("Tags").items():
                tag = {"Key": Key, "Value": Value}
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("sqs").get("queues").remove(queue)

        for table in resources.get("dynamoDB").get("tables"):
            found = False
            for tag in table.get("Tags"):
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("dynamoDB").get("tables").remove(table)

        for acl in resources.get("ec2").get("networkAcls"):
            found = False
            for tag in acl.get("Tags"):
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("ec2").get("networkAcls").remove(acl)
            
        for endpoint in resources.get("ec2").get("vpcEndpoints"):
            found = False
            for tag in endpoint.get("Tags"):
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("ec2").get("vpcEndpoints").remove(endpoint)

        for transit in resources.get("ec2").get("transitGateways"):
            found = False
            for tag in transit.get("Tags"):
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("ec2").get("vpcEndpoints").remove(transit)
        
        for internet in resources.get("ec2").get("internetGateways"):
            found = False
            for tag in internet.get("Tags"):
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("ec2").get("internetGateways").remove(internet)

        for volume in resources.get("ec2").get("volumes"):
            found = False
            for tag in volume.get("Tags"):
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("ec2").get("volumes").remove(volume)

        for nat in resources.get("ec2").get("natGateways"):
            found = False
            for tag in nat.get("Tags"):
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("ec2").get("natGateways").remove(nat)
                
        for route in resources.get("ec2").get("routeTables"):
            found = False
            for tag in route.get("Tags"):
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("ec2").get("routeTables").remove(route)

        for instance in resources.get("ec2").get("instances"):
            found = False
            for nested_data in instance.get("Instances"):
                for tag in nested_data.get("Tags"):
                    tag_found = checkForTag(tag, filters)
                    if tag_found:
                        found = True
            if not found:
                resources.get("ec2").get("instances").remove(instance)

        for topic in resources.get("sns").get("topics"):
            found = False
            for tag in topic.get("Tags"):
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("sns").get("topics").remove(topic)

        for target in resources.get("alb").get("targetGroups"):
            found = False
            for tag in target.get("Tags"):
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("alb").get("targetGroups").remove(target)

        for lb in resources.get("alb").get("loadBalancersV2"):
            found = False
            for tag in lb.get("Tags"):
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("alb").get("loadBalancersV2").remove(lb)

        for bucket in resources.get("s3").get("buckets"):
            found = False
            for tag in bucket.get("TagSet"):
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("s3").get("buckets").remove(bucket)

        for cluster in resources.get("redshift").get("clusters"):
            found = False
            for tag in cluster.get("Tags"):
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("redshift").get("clusters").remove(cluster)

        for function in resources.get("lambda").get("functions"):
            found = False
            for Key, Value in function.get("Tags").items():
                tag = {"Key": Key, "Value": Value}
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("lambda").get("functions").remove(function)

        for activity in resources.get("stepfunctions").get("activities"):
            found = False
            for tag in activity.get("tags"):
                tag["Key"] = tag.get("key")
                tag["Value"] = tag.get("value")
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("stepfunctions").get("activities").remove(activity)

        for stateMachine in resources.get("stepfunctions").get("stateMachines"):
            found = False
            for tag in stateMachine.get("tags"):
                tag["Key"] = tag.get("key")
                tag["Value"] = tag.get("value")
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("stepfunctions").get("stateMachines").remove(stateMachine)

        for group in resources.get("autoscaling").get("groups"):
            found = False
            for tag in group.get("Tags"):
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("autoscaling").get("groups").remove(group)

        for gateway in resources.get("apigateway").get("restApis"):
            found = False
            for Key, Value in gateway.get("tags").items():
                tag = {"Key": Key, "Value": Value}
                tag_found = checkForTag(tag, filters)
                if tag_found:
                    found = True
            if not found:
                resources.get("apigateway").get("restApis").remove(gateway)

    return account


def gatherTags(account):
    account_tags = {}
    for hostedZone in account.get("resources").get("route53").get("hostedZones"):
        for tag in hostedZone.get("Tags"):
            account_tags = addTag(tag, account_tags)

    for cloudFront in account.get("resources").get("cloudFront").get("distributions"):
        for tag in cloudFront.get("Tags").get("Items"):
            account_tags = addTag(tag, account_tags)

    for customerGateway in account.get("resources").get("ec2").get("customerGateways"):
        for tag in customerGateway.get("Tags"):
            account_tags = addTag(tag, account_tags)

    for vpcPeeringCon in account.get("resources").get("ec2").get("vpcPeeringConnections"):
        for tag in vpcPeeringCon.get("Tags"):
            account_tags = addTag(tag, account_tags)

    for vpnGateway in account.get("resources").get("ec2").get("vpnGateways"):
        for tag in vpnGateway.get("Tags"):
            account_tags = addTag(tag, account_tags)

    for vpnCon in account.get("resources").get("ec2").get("vpnConnections"):
        for tag in vpnCon.get("Tags"):
            account_tags = addTag(tag, account_tags)

    for region in account.get("regions"):
        resources = region.get("resources")
        for elb in resources.get("elb").get("loadBalancers"):
            for tag in elb.get("Tags"):
                account_tags = addTag(tag, account_tags)

        for apigate in resources.get("apigatewayv2").get("apis"):
            for Key, Value in apigate.get("Tags").items():
                tag = {"Key": Key, "Value": Value}
                account_tags = addTag(tag, account_tags)

        for cluster in resources.get("rds").get("dbClusters"):
            for tag in cluster.get("TagList"):
                account_tags = addTag(tag, account_tags)

        for instance in resources.get("rds").get("dbInstances"):
            for tag in instance.get("TagList"):
                account_tags = addTag(tag, account_tags)

        for queue in resources.get("sqs").get("queues"):
            for Key, Value in queue.get("Tags").items():
                tag = {"Key": Key, "Value": Value}
                account_tags = addTag(tag, account_tags)

        for table in resources.get("dynamoDB").get("tables"):
            for tag in table.get("Tags"):
                account_tags = addTag(tag, account_tags)

        for vpc in resources.get("ec2").get("vpcs"):
            for tag in vpc.get("Tags"):
                account_tags = addTag(tag, account_tags)

        for acl in resources.get("ec2").get("networkAcls"):
            for tag in acl.get("Tags"):
                account_tags = addTag(tag, account_tags)
            
        for endpoint in resources.get("ec2").get("vpcEndpoints"):
            for tag in endpoint.get("Tags"):
                account_tags = addTag(tag, account_tags)

        for transit in resources.get("ec2").get("transitGateways"):
            for tag in transit.get("Tags"):
                account_tags = addTag(tag, account_tags)
        
        for internet in resources.get("ec2").get("internetGateways"):
            for tag in internet.get("Tags"):
                account_tags = addTag(tag, account_tags)

        for volume in resources.get("ec2").get("volumes"):
            for tag in volume.get("Tags"):
                account_tags = addTag(tag, account_tags)

        for subnet in resources.get("ec2").get("subnets"):
            for tag in subnet.get("Tags"):
                account_tags = addTag(tag, account_tags)

        for nat in resources.get("ec2").get("natGateways"):
            for tag in nat.get("Tags"):
                account_tags = addTag(tag, account_tags)
                
        for route in resources.get("ec2").get("routeTables"):
            for tag in route.get("Tags"):
                account_tags = addTag(tag, account_tags)

        for security in resources.get("ec2").get("securityGroups"):
            for tag in security.get("Tags"):
                account_tags = addTag(tag, account_tags)
        
        for instance in resources.get("ec2").get("instances"):
            for nested_data in instance.get("Instances"):
                for tag in nested_data.get("Tags"):
                    account_tags = addTag(tag, account_tags)

        for topic in resources.get("sns").get("topics"):
            for tag in topic.get("Tags"):
                account_tags = addTag(tag,account_tags)

        for target in resources.get("alb").get("targetGroups"):
            for tag in target.get("Tags"):
                account_tags = addTag(tag,account_tags)

        for lb in resources.get("alb").get("loadBalancersV2"):
            for tag in lb.get("Tags"):
                account_tags = addTag(tag,account_tags)

        for bucket in resources.get("s3").get("buckets"):
            for tag in bucket.get("TagSet"):
                account_tags = addTag(tag,account_tags)

        for cluster in resources.get("redshift").get("clusters"):
            for tag in cluster.get("Tags"):
                account_tags = addTag(tag,account_tags)

        for function in resources.get("lambda").get("functions"):
            for Key, Value in function.get("Tags").items():
                tag = {"Key": Key, "Value": Value}
                account_tags = addTag(tag, account_tags)

        for activity in resources.get("stepfunctions").get("activities"):
            for tag in activity.get("tags"):
                tag["Key"] = tag.get("key")
                tag["Value"] = tag.get("value")
                account_tags = addTag(tag, account_tags)

        for stateMachine in resources.get("stepfunctions").get("stateMachines"):
            for tag in stateMachine.get("tags"):
                tag["Key"] = tag.get("key")
                tag["Value"] = tag.get("value")
                account_tags = addTag(tag, account_tags)

        for group in resources.get("autoscaling").get("groups"):
            for tag in group.get("Tags"):
                account_tags = addTag(tag, account_tags)

        for gateway in resources.get("apigateway").get("restApis"):
            for Key, Value in gateway.get("tags").items():
                tag = {"Key": Key, "Value": Value}
                account_tags = addTag(tag, account_tags)

    return account_tags

def addTag(tag, account_tags):
    if account_tags.get(tag.get("Key")) and (tag.get("Value") not in account_tags.get(tag.get("Key"))):
        account_tags.get(tag.get("Key")).append(tag.get("Value"))
    else:
        account_tags[tag.get("Key")] = [tag.get("Value")]

    return account_tags

def checkForTag(tag, filters):
    for filter_pair in filters:
        if (tag.get("Key") == filter_pair.get("key")) and (tag.get("Value") == filter_pair.get("value")):
            return True
    return False

if __name__ == "__main__":
    filterPrep()
